==================
== Rogue Colors ==
==== Read  Me ====
==================

(Nothing, too fancy right now)

1) Premise
    This game is a rogue-like, because I wanted it to be. I wanted it to be simple so I could focus most of my attention on mechanics in the game. It is full of colors and simple shapes, hence the name. I also wanted it to be easily extendable, because I am slightly lazy and don't want to hard code some things.
2) Requirements
    You need python, that's a given, python2 preferably (python3 compatibility not checked yet). You also need pygame. Other than that, you should be able to run this on a toaster, until it gets dwarf fortress complicated.
3) Controls
    Arrows keys => do movements, menu navigation
    Enter key => Select menu items
    Escape => Ends the program in main menu, exits menus in game and opens the game menu in game
    I key => opens and closes inventory
4) Inspirations
    Dwarf Fortress by Toady for the simplicity in graphics
    Rogue-like games, for the great openness and RNG and simplicity
5) Credits
    Python, for the language I scripted it in and ease of use when I feel lazy
    Pygame, for the ease of making it happen, because I can be lazy
    My parents, for not killing me when I ignore them over coding
    For friends and family that have inspired me
    The internet for the stories of people struggling and then getting something done
