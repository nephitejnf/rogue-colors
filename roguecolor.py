#!/bin/python2

import xml.etree.ElementTree as ETree
import pygame as pg
from pygame.locals import *
import sys
import random

debugmode = False

WINWIDTH = 672
WINHEIGHT = 480
FPS = 30
MOVETICKS = 2

UP = 'up'
DOWN = 'down'
LEFT = 'left'
RIGHT = 'right'

FLOOR = 'floor'
WALL = 'wall'
DOOR = 'door'
NONE = 'none'

def main():
    global FPSCLOCK, DISPLAYSURF, COLORDICT, BASEFONT, TEXTCOLOR, symbolDict
    global PLAYER, roomDict, roomList, finishedgame
    global itemDict, itemList, floorList, stairLoc
    global floorLevel

    # Get our initialization going
    print 'Initialized!'
    pg.init()
    FPSCLOCK = pg.time.Clock()

    # Window setup
    print 'Setting Variables'
    DISPLAYSURF = pg.display.set_mode((WINWIDTH, WINHEIGHT))
    pg.display.set_caption('Rogue Colors')
    BASEFONT = pg.font.Font('RoG.ttf', 20)
    global startpos
    startpos = True

    print 'Setting Dictionaries'
    COLORDICT = {'grass': (0, 100, 30),
                 'wall': (100, 100, 100),
                 'wall center': (70, 70, 70),
                 'stone floor': (125, 125, 125),
                 'door': (90, 60, 0),
                 'player': (255, 50, 0)}
    TEXTCOLOR = {'orange': (255, 50, 0),
                 'blue': (35, 130, 255),
                 'black': (0, 0, 0),
                 'white': (255, 255, 255),
                 'red': (200, 0, 0),
                 'sage green': (30, 170, 35)}
    # This section sets our room and floor items
    symbolDict = {'#': WALL,
                  '.': FLOOR,
                  'D': DOOR,
                  '_': NONE}
    '''
    roomDict = {'O': ['########',
                      '#......#',
                      '#......#',
                      '#......#',
                      '#......#',
                      '########'],
                '-': ['________',
                      '________',
                      '########',
                      '........',
                      '########',
                      '________'],
                '+': ['___#.#__',
                      '___#.#__',
                      '####.###',
                      '........',
                      '####.###',
                      '___#.#__'],
                '|': ['___#.#__',
                      '___#.#__',
                      '___#.#__',
                      '___#.#__',
                      '___#.#__',
                      '___#.#__'],
                '0': ['########',
                      '#......#',
                      '#..##..#',
                      '#..##..#',
                      '#......#',
                      '########']}'''
    #roomList = ['O', '-', '+', '|', '0']
    roomDict = initRooms()
    roomList = roomDict.keys()
    floorList = []
    stairLoc = (0, 0)
    floorLevel = 1
    lastTick = 0

    global enemylist
    enemylist = initEnemies()
    initItems()

    # Player values
    PLAYER = {'x': 4,
              'y': 4,
              'health': 10,
              'maxhp': 10,
              'stats': {'atk': 2, 'per': 5, 'ac': 0, 'wis': 5},
              'inventory': [],
              'equipment':{'weapon': {'name': 'Empty'},
                  'shield': {'name': 'Empty'},
                  'armor': {'name': 'Empty'}},
              'direction': ' '}

    while True:
        # The game loop starts here
        finishedgame = False
        startScreen()
        playingGame(floorLevel, lastTick)

# Our main game starts here, this is also called recusively
# It shouldn't pass one thousand as we are only working with 20 levels
# It also starts over once we hit gameover or wim the game
def playingGame(floorLev, lastTick=0):
    global floorLevel
    floorLevel = floorLev
    global maps
    maps = genFloor()
    findClearSpot()
    global enemyLocs
    enemyLocs = spawnEnemyLocations(random.randint(10, 15))
    itemLocs(6, 2, 3, 3)
    # Another loop (inside a loop) that runs the game
    while True:
        lastTick, finishedGame, menu = playgame(lastTick)
        if finishedgame or menu is 'main': break
        pg.display.update()
        FPSCLOCK.tick(FPS)

# This resets the player values when you start the game all over.
def resetPlayer():
    if debugmode: print 'Setting player stats'
    PLAYER['health'] = 10
    PLAYER['maxhp'] = 10

# This loads up our items into lists
def initItems():
    print 'Creating items'
    loader = ETree.parse('assets/items.xml').getroot()
    global items
    items = {'potions': [potionParse(potion) for potion in loader.find('items').findall('hp-potion')],
             'weapons': [weaponParse(weapon) for weapon in loader.find('items').findall('weapon')],
             'armor': [armorParse(armor) for armor in loader.find('items').findall('armor')],
             'shield': [shieldParse(armor) for armor in loader.find('items').findall('shield')]}
    if True:
        print items['potions']
        print items['weapons']
        print items['armor']
        print items['shield']

def initRooms():
    print 'Loading Rooms'
    loader = ETree.parse('assets/rooms.xml').getroot()
    roomses = {}
    for room in loader.find('rooms').findall('room'):
        roomed = roomParse(room)
        roomie = roomed.keys()
        roomses[roomie[0]] = roomed.values()[0]
    print roomses
    return roomses


# This loads up enemies
def initEnemies():
    print 'Creating enemy list'
    loader = ETree.parse('assets/enemies.xml').getroot()
    enemies = [enemyParser(enemy) for enemy in loader.find('enemies').findall('enemy')]
    print enemies
    return enemies

# These functions parse the xml into dictionaries
def weaponParse(weapon):
    name = weapon.find('name').text
    desc = weapon.find('desc').text
    kind = weapon.find('type').text
    dam = int(weapon.find('damage').text)
    diction = {'name': name, 'description': desc, 'type': kind, 'damage': dam}
    return diction

def potionParse(potion):
    name = potion.find('name').text
    desc = potion.find('desc').text
    heal = int(potion.find('heal').text)
    diction = {'name': name, 'description': desc, 'heal': heal, 'type': 'potion'}
    return diction

def armorParse(armor):
    name = armor.find('name').text
    desc = armor.find('desc').text
    ar = int(armor.find('rating').text)
    diction = {'name': name, 'description': desc, 'rating': ar, 'type': 'armor'}
    return diction

def shieldParse(shield):
    name = shield.find('name').text
    desc = shield.find('desc').text
    ar = int(shield.find('rating').text)
    diction = {'name': name, 'description': desc, 'rating': ar, 'type': 'shield'}
    return diction

def roomParse(room):
    lett = room.find('type').text
    roomarr = room.find('layout')
    nexter = [row for row in roomarr.findall('row')]
    diction = {lett: [row.text for row in nexter]}
    print diction[lett]
    return diction
# End of parsers, probably didn't need to do this

# Color parsing
def colorParser(xcolor):
    r = int(xcolor.find('r').text)
    g = int(xcolor.find('g').text)
    b = int(xcolor.find('b').text)
    return (r,g,b)

# Parses enemies
def enemyParser(enemy):
    name = enemy.find('name').text
    color = colorParser(enemy.find('color'))
    health = int(enemy.find('hp').text)
    level = int(enemy.find('start-level').text)
    dam = int(enemy.find('damage').text)
    diction = {'name': name, 'color': color, 'health': health, 'level': level, 'damage': dam}
    return diction

# Lets set up some item locations
def itemLocs(potionNumber, weaponNumber, armorNumber, shieldNumber):
    print 'Locationalizing items'
    global itemlist
    itemlist = []
    for i in range(0, potionNumber):
        itemlist.append((random.choice(items['potions']), clearLocation()))
    for i in range(0, weaponNumber):
        itemlist.append((random.choice(items['weapons']), clearLocation()))
    for i in range(0, armorNumber):
        itemlist.append((random.choice(items['armor']), clearLocation()))
    for i in range(0, shieldNumber):
        itemlist.append((random.choice(items['shield']), clearLocation()))
    if debugmode:
        for item in itemlist: print item

# It always come back to this when the loop starts again
# It's not that fancy, but it gets the job done
def startScreen():
    #Syntactic sugar
    print 'Adding sugar to start screen'
    bgcolor = TEXTCOLOR['orange']
    titlecolor = TEXTCOLOR['sage green']
    titlebackdrop = TEXTCOLOR['black']
    optionscolor = TEXTCOLOR['red']
    #Fonts for the screen
    print 'Adding fonts to start screen'
    titlefont = pg.font.Font('RoG.ttf', 80)
    optionfont = pg.font.Font('RoG.ttf', 30)

    print 'Creating text'
    title1 = titlefont.render('Rogue Colors', 1, titlebackdrop)
    title2 = titlefont.render('Rogue Colors', 1, titlecolor)

    title1rect = title1.get_rect()
    title1rect.centerx = (WINWIDTH/2) + 5
    title1rect.top = 45

    title2rect = title2.get_rect()
    title2rect.centerx = WINWIDTH/2
    title2rect.top = 40

    print 'Creating main menu options'
    playSelect = optionfont.render('Play', 1, optionscolor)
    playRect = playSelect.get_rect()
    playRect.center = (WINWIDTH/2, (WINHEIGHT/2)-20)

    quitSelect = optionfont.render('Quit', 1, optionscolor)
    quitRect = quitSelect.get_rect()
    quitRect.center = (WINWIDTH/2, (WINHEIGHT/2)+10)

    selector = 'play'
    selectory = (WINHEIGHT/2)-38

    print 'looping'
    while True:
        for event in pg.event.get():
            if event.type == QUIT:
                quitme()
            #elif event.type == KEYUP:

            elif event.type == KEYDOWN:
                if event.key == K_ESCAPE:
                    quitme()
                #The cheesy way to pick you choices
                elif event.key in (K_UP, K_DOWN):
                    if selector == 'play':
                        selectory = (WINHEIGHT/2)-7
                        selector = 'quit'
                    elif selector == 'quit':
                        selectory = (WINHEIGHT/2)-38
                        selector = 'play'
                elif event.key == K_RETURN:
                    if selector == 'quit':
                        quitme()
                    elif selector == 'play':
                        resetPlayer()
                        return



        DISPLAYSURF.fill(bgcolor)
        DISPLAYSURF.blit(title1, title1rect)
        DISPLAYSURF.blit(title2, title2rect)
        DISPLAYSURF.blit(playSelect, playRect)
        DISPLAYSURF.blit(quitSelect, quitRect)
        pg.draw.rect(DISPLAYSURF, optionscolor, ((WINWIDTH/2)-50, selectory, 100, 35), 3)

        pg.display.update()
        FPSCLOCK.tick(FPS)

# Drawing our health display, Legend of Zelda style (minus the hearts)
def drawHealth():
    halfbar = False
    o = 0
    counter = 0
    if PLAYER['health']%2 == 1:
        halfbar = True
    while (o < PLAYER['maxhp']-1):
        pg.draw.rect(DISPLAYSURF, TEXTCOLOR['black'], (10+(12*counter), 10, 10, 10), 2)
        counter+=1
        o+=2
    o, counter = 0, 0
    while (o < PLAYER['health']-1):
        pg.draw.rect(DISPLAYSURF, TEXTCOLOR['red'], ((12*counter)+12, 12, 10, 10))
        counter+=1
        o+=2
    if halfbar:
        pg.draw.rect(DISPLAYSURF, TEXTCOLOR['red'], ((12*counter)+15, 15,  4, 4))

# Equiping items in their proper slot
def equipItem(item):
    typed = ''
    if item in items['weapons']:
        typed = 'weapon'
    elif item in items['armor']:
        typed = 'armor'
    elif item in items['shield']:
        typed = 'shield'
    placer = PLAYER['equipment'][typed]
    PLAYER['equipment'][typed] = item
    if typed.startswith('ar') or typed.startswith('sh'):
        PLAYER['stats']['ac'] += item['rating']
    if placer['name'] != 'Empty':
        if typed.startswith('ar') or typed.startswith('sh'):
            PLAYER['stats']['ac'] -= placer['rating']
        PLAYER['inventory'].append(placer)
    tossItem(item)

# Tossing items
def tossItem(item):
    if debugmode: print 'Tossed %s' % item['name']
    popping = PLAYER['inventory'].index(item)
    PLAYER['inventory'].pop(popping)

# The item dialog
def itemDialog(item):
    dialoglist = ['Equip', 'Toss', 'Cancel']
    top = (WINHEIGHT/2)+30
    df = pg.font.Font('RoG.ttf', 16)
    if item['type']=='potion':
        dialoglist[0] = 'Drink'
    nagged = True
    indicator = 0
    while nagged:
        pg.draw.rect(DISPLAYSURF, TEXTCOLOR['black'], (30, top, 100, 80), 3)
        pg.draw.rect(DISPLAYSURF, TEXTCOLOR['blue'], (33, top+3, 94, 74))
        i = 0
        for itemd in dialoglist:
            box = df.render(itemd, 0, TEXTCOLOR['black'])
            boxRect = box.get_rect()
            boxRect.topleft = (60, (i*15)+(top+10))
            i+=1
            DISPLAYSURF.blit(box, boxRect)

        for event in pg.event.get():
            if event.type==QUIT:
                quitme()
            elif event.type==KEYDOWN:
                if event.key==K_ESCAPE:
                    nagged = False
                elif event.key==K_RETURN:
                    if indicator==2:
                        nagged = False
                    elif indicator==1:
                        tossItem(item)
                        nagged = False
                    elif indicator==0:
                        if dialoglist[0]=='Drink':
                            healPlayer(item['heal'])
                            tossItem(item)
                            nagged = False
                        elif dialoglist[0]=='Equip':
                            equipItem(item)
                            nagged = False
                elif event.key==K_UP:
                    indicator -= 1
                    if indicator < 0:
                        indicator = 2
                elif event.key==K_DOWN:
                    indicator += 1
                    if indicator > 2:
                        indicator = 0

        y = (top+12)+(indicator*15)
        x = 40
        tri = [(x, y), (x+10, y+4), (x, y+8)]
        pg.draw.polygon(DISPLAYSURF, TEXTCOLOR['black'], tri)

        pg.display.update()
        FPSCLOCK.tick(FPS)

# Creating something to draw our equipment list
def drawEquipList():
    top = 40
    left = (WINWIDTH/2)+20
    EquipFont = pg.font.Font('RoG.ttf', 16)
    weaponEquip = EquipFont.render('Weapon: %s' % PLAYER['equipment']['weapon']['name'], 0, TEXTCOLOR['black'])
    weaponRect = weaponEquip.get_rect()
    weaponRect.topleft = (left, top)
    shieldEquip = EquipFont.render('Shield: %s' % PLAYER['equipment']['shield']['name'], 0, TEXTCOLOR['black'])
    shieldRect = shieldEquip.get_rect()
    shieldRect.topleft = (left, top+20)
    armorEquip = EquipFont.render('Armor: %s' % PLAYER['equipment']['armor']['name'], 0, TEXTCOLOR['black'])
    armorRect = armorEquip.get_rect()
    armorRect.topleft = (left, top+40)
    DISPLAYSURF.blit(weaponEquip, weaponRect)
    DISPLAYSURF.blit(shieldEquip, shieldRect)
    DISPLAYSURF.blit(armorEquip, armorRect)

# Draw some stats for us to see
def drawStats():
    top = 100
    left = (WINWIDTH/2)+20
    EquipFont = pg.font.Font('RoG.ttf', 16)
    titFint = pg.font.Font('RoG.ttf', 18)
    titleStats = titFint.render("  ", 0, TEXTCOLOR['black'])
    titleRect = titleStats.get_rect()
    titleRect.topleft = (left, top)
    hprender = 'HP: {} : {}'
    hpBox = EquipFont.render(hprender.format(PLAYER['health'], PLAYER['maxhp']), 0, TEXTCOLOR['black'])
    hpRect = hpBox.get_rect()
    hpRect.topleft = (left, top+25)
    acBox = EquipFont.render('Armor Class: %d' % PLAYER['stats']['ac'], 0, TEXTCOLOR['black'])
    acRect = acBox.get_rect()
    acRect.topleft = (left, top+45)
    damage = PLAYER['stats']['atk']
    if PLAYER['equipment']['weapon']['name'] != 'Empty':
        damage += PLAYER['equipment']['weapon']['damage']
    damBox = EquipFont.render('Damage: %d' % damage, 0, TEXTCOLOR['black'])
    damRect = damBox.get_rect()
    damRect.topleft = (left, top+65)
    DISPLAYSURF.blit(titleStats, titleRect)
    DISPLAYSURF.blit(hpBox, hpRect)
    DISPLAYSURF.blit(acBox, acRect)
    DISPLAYSURF.blit(damBox, damRect)

# Our sexy inventory screen
def drawInventoryScreen():
    nagger = True
    indicator = 0
    smallerfont = pg.font.Font('RoG.ttf', 16)
    while nagger:
        dialoged = False
        pg.draw.rect(DISPLAYSURF, TEXTCOLOR['black'], (30, 30, (WINWIDTH-60), (WINHEIGHT-60)), 4)
        pg.draw.rect(DISPLAYSURF, TEXTCOLOR['blue'], (33, 33, (WINWIDTH-66), (WINHEIGHT-66)))
        if len(PLAYER['inventory']) != 0:
            for l in range(0, len(PLAYER['inventory'])):
                pg.draw.rect(DISPLAYSURF, TEXTCOLOR['white'], (40, (40+(5*l)), 10, 3))


        for event in pg.event.get():
            if event.type == QUIT:
                quitme()
            elif event.type == KEYDOWN:
                if event.key == K_i:
                    nagger = False
                elif event.key == K_ESCAPE:
                    nagger = False
                elif event.key == K_RETURN:
                    dialoged = True
                elif event.key == K_UP and len(PLAYER['inventory']) != 0:
                    indicator -= 1
                    if indicator < 0:
                        indicator = len(PLAYER['inventory'])-1
                elif event.key == K_DOWN and len(PLAYER['inventory']) != 0:
                    indicator += 1
                    if indicator == len(PLAYER['inventory']):
                        indicator = 0

        pg.draw.rect(DISPLAYSURF, TEXTCOLOR['black'], (40, (40+(5*indicator)), 10, 3))
        if len(PLAYER['inventory']) != 0:
            try:
                itemName = BASEFONT.render(PLAYER['inventory'][indicator]['name'], 1, TEXTCOLOR['black'])
                itemDesc = BASEFONT.render(PLAYER['inventory'][indicator]['description'], 1, TEXTCOLOR['black'])
            except:
                itemName = BASEFONT.render(PLAYER['inventory'][indicator-1]['name'], 1, TEXTCOLOR['black'])
                itemDesc = BASEFONT.render(PLAYER['inventory'][indicator-1]['description'], 1, TEXTCOLOR['black'])
            descLabel = smallerfont.render('Description', 1, TEXTCOLOR['black'])
            desclrect = descLabel.get_rect()
            desclrect.topleft = (90, (WINHEIGHT/2)-20)
            DISPLAYSURF.blit(descLabel, desclrect)
            itemRect = itemName.get_rect()
            descRect = itemDesc.get_rect()
            itemRect.topleft = (60, 40)
            descRect.topleft = (60, WINHEIGHT/2)
            DISPLAYSURF.blit(itemName, itemRect)
            DISPLAYSURF.blit(itemDesc, descRect)
        drawEquipList()
        drawStats()
        if dialoged:
            itemDialog(PLAYER['inventory'][indicator])
            indicator = 0
        pg.display.update()
        FPSCLOCK.tick(FPS)
    return

# Basically putting down where the stairs to go to the next level go
def drawStair(coords):
    xx, yy = coords
    x = ((WINWIDTH/2)-(PLAYER['x']*32)+16)+((xx-1)*32)
    y = ((WINHEIGHT/2)-(PLAYER['y']*32)+16)+((yy-1)*32)
    pg.draw.lines(DISPLAYSURF, TEXTCOLOR['black'], False, [(x, y+31),(x, y),(x+31, y)])
    pg.draw.polygon(DISPLAYSURF, TEXTCOLOR['black'], [(x+31, y), (x, y+31), (x+31, y+31)])

def genFloor():
    print 'Creating rooms for you floor' #This method creates the room scheme
    tempList = []
    for y in range(0, 10):
        linelist = []
        linestring = ''
        for x in range(0, 10):
            linestring += random.choice(roomList)
        tempList.append(linestring)
    floorList.append(tempList)
    floorLevel = len(floorList)
    return createMap(tempList)

# Yeah, a whole other method to take damage
def takeDamage(dmg):
    health = PLAYER['health']
    dmg = dmg - PLAYER['stats']['ac']
    if dmg < 0: dmg = 0
    PLAYER['health'] = health - dmg
    if debugmode: print PLAYER['health']

def healPlayer(heal):
    health = PLAYER['health']
    PLAYER['health'] = health + heal

def createMap(rooms): #Converts the room floor plan to the full plan
    maplines = []
    for lines in rooms: # room rows
        for rline in range(0, 6): # lines in rooms
            s = ''
            for room in lines: # rooms
                s += roomDict[room][rline] # creating the whole map, line by line
            maplines.append(s)
    print 'Closing off edges'
    mapreplace = maplines[0].replace('.', '#') #top row add wals for ends
    maplines[0] = mapreplace
    mapbotreplace = maplines[len(maplines)-1].replace('.', '#') #bottom wall
    maplines[len(maplines)-1] = mapbotreplace
    colr = len(maplines[0])-1
    tempind = 0
    for line in maplines: #creates wall in the sides
        if line.startswith('.'):
            line = '#'+line[1:]
        if line.endswith('.'):
            line = line[0:len(line)-1]+'#'
        maplines[tempind] = line
        tempind += 1
    temprow = 0
    print 'Adding doors and dead ends'
    for row in maplines: #walls off dead ends and adds doors to room connections
        indextemp = 0
        if temprow not in (0, len(maplines)-1):
            for char in row:
                if indextemp not in (0, len(maplines[temprow])-1):
                    if char=='#' and ((maplines[temprow-1][indextemp]=='.' \
                            and maplines[temprow+1][indextemp]=='.')):
                            row = row[:indextemp]+'D'+row[indextemp+1:]
                    if char=='#' and (maplines[temprow][indextemp-1]=='.' \
                            and maplines[temprow][indextemp+1]=='.'):
                            row = row[:indextemp]+'D'+row[indextemp+1:]
                    if char=='.' and (maplines[temprow-1][indextemp]=='_' or \
                            maplines[temprow+1][indextemp]=='_' or\
                            maplines[temprow][indextemp+1]=='_' or\
                            maplines[temprow][indextemp-1]=='_'):
                            row = row[:indextemp]+'#'+row[indextemp+1:]
                indextemp += 1
            maplines[temprow] = row
        temprow += 1
    flagged = False
    while not flagged: #This decides the location of out stairs
        xx = random.randint(1, len(maplines[1])-2) #random coordinates
        yy = random.randint(1, len(maplines)-2)
        global stairLoc
        stairLoc = (xx, yy)
        if symbolDict[maplines[yy][xx]] == FLOOR: flagged = True #If it is a floor we skip
        if floorLevel > 19: stairLoc = [1000, 1000] #If we hit larger than 19, no stairs
    flagged = False #Resetting the flag
    if debugmode: #We have lots of these debug flags
        for line in maplines:
            print line
    return maplines

def clearLocation():
    flagged = False
    loc = (0, 0)
    while not flagged:
        xx = random.randint(1, len(maps[1])-2)
        yy = random.randint(1, len(maps)-2)
        loc = (xx, yy)
        if symbolDict[maps[yy][xx]] == FLOOR: flagged = True
    return loc

# Randomly selects locations for our enemies
def spawnEnemyLocations(num):
    print 'Placing Enemies'
    enemyloclist = []
    for x in range(0, num):
        flager = False #Another flagging variable
        while flager==False:
            ex = random.randint(1, len(maps[1])-2)
            ey = random.randint(1, len(maps)-2)
            flager = checkEnemySpot(ex, ey)
            if (ex, ey) in enemyloclist: #Makes sure we don't have an over lapping enemy
                flager = False
        enemyloclist.append([(ex, ey), randomEnemy()])
    if debugmode: #Debug flags
        print 'Number of enemies created: '+str(len(enemyloclist))
        for item in enemyloclist:
            print 'Enemy locations:'
            print item
    return enemyloclist

def randomEnemy():
    chosen_one = random.choice(enemylist)
    stag = True
    while stag:
        if chosen_one['level'] <= floorLevel:
            stag = False
        else:
            chosen_one = random.choice(enemylist)
    return chosen_one

# This method checks whether the spot is an open location
def checkEnemySpot(x, y):
    if symbolDict[maps[y][x]] in (FLOOR, DOOR):
        return True
    elif symbolDict[maps[y][x]] in (NONE, WALL):
        return False

# This one randomly selects a direction to move, enemies can move diagonally
def enemyMove(locations):
    for loc in locations:
        x, y = loc[0]
        movex, movey = 0, 0
        flags = False #Invalid flag variable
        while flags == False:
            movex = random.choice([-1, 0, 1])
            movey = random.choice([-1, 0, 1])
            flags = checkEnemySpot(x+movex, y+movey) #This checks the spot, sets the flag
        spotr = enemyLocs.index(loc)
        loc[0] = (movex+x, movey+y)
        enemyLocs[spotr] = loc

# Drawing our enemies
def drawEnemies(color, locations):
    for location in locations:
        modx, mody = location[0]
        x = (WINWIDTH/2)+((modx-PLAYER['x'])*32)+16
        y = (WINHEIGHT/2)+((mody-PLAYER['y'])*32)+16
        tricoord = [(x-2, y-30), (x-16, y-2), (x-30, y-30)]
        pg.draw.polygon(DISPLAYSURF, location[1]['color'], tricoord)

# Draw the items ont he floor
def drawItems(items):
    black = (0, 0, 0)
    white = (255, 255, 255)
    red = (250, 0, 0)
    for item in items:
        xx, yy = item[1]
        x = (WINWIDTH/2)+((xx-PLAYER['x'])*32)+16
        y = (WINHEIGHT/2)+((yy-PLAYER['y'])*32)+16
        center = (x-16, y-16)
        pg.draw.circle(DISPLAYSURF, black, center, 15, 3)
        pg.draw.circle(DISPLAYSURF, white, center, 13, 8)
        pg.draw.circle(DISPLAYSURF, red, center, 4)

# Drawing our whole floor
def drawFloor(floor):
    x, y = 0, 0
    for line in floor: #Line by line
        for chara in line: #Tile by tile
            drawTile(symbolDict[chara], x-PLAYER['x'], y-PLAYER['y'])
            x += 1
        y += 1
        x = 0
    drawStair((stairLoc))

# Draws an individual tile at the decided coordinates
def drawTile(tileType, x, y):
    color1, color2 = None, None
    drawx = ((WINWIDTH/2)-(PLAYER['x']*32)+16)+((x-1)*32)
    drawy = ((WINHEIGHT/2)-(PLAYER['y']*32)+16)+((y-1)*32)
    if tileType==WALL:
        color1 = COLORDICT['wall']
        color2 = COLORDICT['wall center']
    elif tileType==FLOOR:
        color1 = COLORDICT['stone floor']
    elif tileType==DOOR:
        color1 = COLORDICT['door']

    if tileType in (WALL, FLOOR, DOOR):
        pg.draw.rect(DISPLAYSURF, color1, ((WINWIDTH/2)+x*32-16, (WINHEIGHT/2)+y*32-16, 32, 32))
    if color2:
        pg.draw.rect(DISPLAYSURF, color2, ((32*x+6)+(WINWIDTH/2)-16, (32*y+6)+(WINHEIGHT/2)-16, 20, 20))

# This places the player in the middle of the screen (Viewport)
def drawPlayer(x, y):
    pg.draw.circle(DISPLAYSURF, COLORDICT['player'], (WINWIDTH/2, WINHEIGHT/2), 16, 0)

# The player needs a clear spot, too
def findClearSpot():
    print 'Finding you a good spot'
    for x in range(1, 5):
        for y in range(1, 5):
            if maps[y][x]=='.':
                PLAYER['y']=y
                PLAYER['x']=x
                return

# This is checking our player's spot
def checkSpot(modx, mody):
    x = PLAYER['x']+modx
    y = PLAYER['y']+mody
    enemyMove(enemyLocs)
    # This particular section gave me trouble, but I forgot to add the
    # condition that limits it inside the map.
    if symbolDict[maps[y][x]]==WALL:
        if x in range(0, len(maps[y])) and y in range(0, len(maps)):
            if PLAYER['direction']==RIGHT and (x+1<=len(maps[y])-1 and
                    symbolDict[maps[y][x+1]]==WALL):
                if symbolDict[maps[y][x+2]]==FLOOR:
                    stringer = maps[y][:x] + '..' + maps[y][x+2:]
                    maps[y] = stringer
                    return False
                else:
                    return False
            elif PLAYER['direction']==LEFT and symbolDict[maps[y][x-1]]==WALL:
                if symbolDict[maps[y][x-2]]==FLOOR:
                    stringer = maps[y][:x-1] + '..' + maps[y][x+1:]
                    maps[y] = stringer
                    return False
                else:
                    return False
            elif PLAYER['direction']==UP and (symbolDict[maps[y-1][x]]==WALL
                    and y>1):
                # This particular part allowed blowing out the top wall.
                # Well, at one time it did, not anymore.
                if symbolDict[maps[y-2][x]]==FLOOR:
                    stringer = maps[y-1][:x] + '.' + maps[y-1][x+1:]
                    maps[y-1] = stringer
                    stringer = maps[y][:x] + '.' + maps[y][x+1:]
                    maps[y]=stringer
                    return False
                else:
                    return False
            elif PLAYER['direction']==DOWN and (y+1<=len(maps)-1 and
                    symbolDict[maps[y+1][x]]==WALL):
                if symbolDict[maps[y+2][x]]==FLOOR:
                    stringer = maps[y][:x] + '.' +maps[y][x+1:]
                    maps[y] = stringer
                    string = maps[y+1][:x] + '.' + maps[y+1][x+1:]
                    maps[y+1] = string
                    return False
                else:
                    return False
            else:
                return False
        else:
            return False
    elif symbolDict[maps[y][x]] in (FLOOR, DOOR, NONE): #Check our location
        if (x, y) == stairLoc:
            print str(floorLevel+1)
            playingGame(floorLevel+1)
        for item in itemlist:
            if (x, y) == item[1]:
                popped = itemlist.index(item)
                PLAYER['inventory'].append(item[0])
                itemlist.pop(popped)
                print PLAYER['inventory']
        for enemy in enemyLocs:
            if (x, y) == enemy[0]:
                popper = enemyLocs.index(enemy)
                test = random.randint(1, 100)
                if test < 50: takeDamage(enemy[1]['damage'])
                if test > 60: enemyTakeDamage(popper, playDamageCalc())
                if debugmode:
                    t = 1
                    for item in enemyLocs:
                        print 'Enemy {0:d} at {1}'.format(t, item[0])
                        t+=1
                    print 'Number of enemies present: ' + str(len(enemyLocs))
                return False
        return True

def playDamageCalc():
    dam = 0
    if PLAYER['equipment']['weapon']['name'] != "Empty":
        dam = PLAYER['equipment']['weapon']['damage']
    return dam + PLAYER['stats']['atk']

def enemyTakeDamage(eindex, damage):
    enemyLocs[eindex][1]['health'] -= damage
    if enemyLocs[eindex][1]['health'] <= 0:
        enemyLocs.pop(eindex)

def movePlayer(direction, x, y, lastTick=0): #Which direction are you going? Not diagonal
    if lastTick > MOVETICKS:
        if direction==' ':
            return x, y, lastTick
        elif direction==RIGHT and checkSpot(1, 0):
            x+=1
            #print '({0:d}, {1:d})'.format(x, y)
            return x, y, 0
        elif direction==LEFT and checkSpot(-1, 0):
            x-=1
            #print '({0:d}, {1:d})'.format(x, y)
            return x, y, 0
        elif direction==UP and checkSpot(0, -1):
            y-=1
            #print '({0:d}, {1:d})'.format(x, y)
            return x, y, 0
        elif direction==DOWN and checkSpot(0, 1):
            y+=1
            #print '{0:d}, {1:d})'.format(x, y)
            return x, y, 0
        else:
            return x, y, lastTick
    else:
        return x, y, lastTick

def playgame(lastTick, men=''): #Most of the mechanics run from here
    DISPLAYSURF.fill(COLORDICT['grass'])
    direct = ' '
    invopen = False
    menued = False
    for event in pg.event.get():
        if event.type == QUIT:
            quitme()
        elif event.type == KEYUP:
            if event.key in (K_UP, K_DOWN, K_RIGHT, K_LEFT):
                PLAYER['direction'] = ' '
        elif event.type == KEYDOWN:
            if event.key == K_ESCAPE:
                menued = True
            elif event.key == K_UP:
                PLAYER['direction'] = UP
            elif event.key == K_DOWN:
                PLAYER['direction'] = DOWN
            elif event.key == K_RIGHT:
                PLAYER['direction'] = RIGHT
            elif event.key == K_LEFT:
                PLAYER['direction'] = LEFT
            elif event.key == K_i:
                invopen = True
            if event.key == K_1 and debugmode:
                takeDamage(1) #Yeah, force yourself to take damage, great debugging for gameover
    PLAYER['x'], PLAYER['y'], lastTick = movePlayer(PLAYER['direction'], PLAYER['x'], PLAYER['y'], lastTick)
    lastTick += 1
    #Our mechanic methods
    drawFloor(maps)
    drawPlayer(PLAYER['x'], PLAYER['y'])
    drawItems(itemlist)
    drawEnemies((150, 0, 10), enemyLocs)
    drawHealth()
    if invopen: drawInventoryScreen()
    done = False #This flag is our gameover, win flag
    if menued: men = drawGameMenu()
    if len(enemyLocs)==0 or PLAYER['health'] == 0: done = True #Set here
    return lastTick, done, men #Passing it onto a higher method

def drawGameMenu():
    options = ['Resume', 'Main Menu', 'Quit']
    mf = pg.font.Font('RoG.ttf', 16)
    top = (WINHEIGHT/2)-30
    left = (WINWIDTH/3)-30
    indicator = 0
    done = ''
    nagged = True
    while nagged:
        pg.draw.rect(DISPLAYSURF, TEXTCOLOR['black'], (left, top, 100, 100), 3)
        pg.draw.rect(DISPLAYSURF, TEXTCOLOR['blue'], (left+2, top+2, 96, 96))
        i = 0
        for itemd in options:
            box = mf.render(itemd, 0, TEXTCOLOR['black'])
            boxRect = box.get_rect()
            boxRect.topleft = (left+30, top+15+(i*20))
            i+=1
            DISPLAYSURF.blit(box, boxRect)
        for event in pg.event.get():
            if event.type==QUIT:
                quitme()
            elif event.type==KEYDOWN:
                if event.key==K_ESCAPE:
                    nagged = False
                elif event.key==K_RETURN:
                    if indicator==2:
                        quitme()
                    elif indicator==1:
                        done = 'main'
                        nagged = False
                    elif indicator==0:
                        done = ' '
                        nagged = False
                elif event.key==K_UP:
                    indicator -= 1
                    if indicator < 0:
                        indicator = 2
                elif event.key==K_DOWN:
                    indicator += 1
                    if indicator > 2:
                        indicator = 0

        y = (top+18)+(indicator*20)
        x = left+5
        tri = [(x, y), (x+10, y+4), (x, y+8)]
        pg.draw.polygon(DISPLAYSURF, TEXTCOLOR['black'], tri)

        pg.display.update()
        FPSCLOCK.tick(FPS)
    return done

def quitme(): #Basic quit function
    pg.quit()
    sys.exit()

if __name__ == '__main__':
    main()
